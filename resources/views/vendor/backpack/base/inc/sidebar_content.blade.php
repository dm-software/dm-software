<!-- Begin Dashboard -->
	<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard" aria-hidden="true"></i> <span>{{ trans('general.dashboard') }}</span></a></li>
<!-- End Dashboard -->
<!-- Begin CMS -->
	@can('Ver CMS')
	<li class="treeview">
		<a href="#"><i class="fa fa-chrome" aria-hidden="true"></i> <span>{{ trans('general.cms') }}</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a>
		<ul class="treeview-menu">
			<li><a href="{{ backpack_url('page') }}"><i class="fa fa-file-o" aria-hidden="true"></i> <span>{{ trans('general.pages') }}</span></a></li>
			<li><a href="{{ backpack_url('menu-item') }}"><i class="fa fa-list" aria-hidden="true"></i> <span>{{ trans('general.menus') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End CMS -->
<!-- Begin News -->
	@can('Ver Noticias')
	<li class="treeview">
		<a href="#"><i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>{{ trans('general.news') }}</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a>
		<ul class="treeview-menu">
			<li><a href="{{ backpack_url('article') }}"><i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>{{ trans('general.articles') }}</span></a></li>
			<li><a href="{{ backpack_url('category') }}"><i class="fa fa-list" aria-hidden="true"></i> <span>{{ trans('general.categories') }}</span></a></li>
			<li><a href="{{ backpack_url('tag') }}"><i class="fa fa-tag" aria-hidden="true"></i> <span>{{ trans('general.tags') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End News -->
<!-- Begin Files -->
	@can('Ver Archivos')
	<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o" aria-hidden="true"></i> <span>{{ trans('general.files') }}</span></a></li>
	@endcan
<!-- End Files -->
<!-- Begin Translations -->
	@can('Ver Traducciones')
	<li class="treeview">
		<a href="#"><i class="fa fa-globe" aria-hidden="true"></i> <span>{{ trans('general.translations') }}</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a>
		<ul class="treeview-menu">
			<li><a href="{{ backpack_url('language') }}"><i class="fa fa-flag-checkered" aria-hidden="true"></i> <span>{{ trans('general.languages') }}</span></a></li>
			<li><a href="{{ backpack_url('language/texts') }}"><i class="fa fa-language" aria-hidden="true"></i> <span>{{ trans('general.language_files') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End Translations -->
<!-- Begin Users -->
	@can('Ver Usuarios')
	<li class="treeview">
		<a href="#"><i class="fa fa-group" aria-hidden="true"></i> <span>{{ trans('general.users') }}</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a>
		<ul class="treeview-menu">
			<li><a href="{{ backpack_url('user') }}"><i class="fa fa-user" aria-hidden="true"></i> <span>{{ trans('general.users') }}</span></a></li>
			<li><a href="{{ backpack_url('role') }}"><i class="fa fa-group" aria-hidden="true"></i> <span>{{ trans('general.roles') }}</span></a></li>
			<li><a href="{{ backpack_url('permission') }}"><i class="fa fa-key" aria-hidden="true"></i> <span>{{ trans('general.permissions') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End Users -->
<!-- Begin Configuration -->
	@can('Ver Configuración')
	<li class="treeview">
		<a href="#"><i class="fa fa-cogs" aria-hidden="true"></i> <span>{{ trans('general.configuration') }}</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a>
		<ul class="treeview-menu">
			<li><a href="{{ backpack_url('setting') }}"><i class="fa fa-cog" aria-hidden="true"></i> <span>{{ trans('general.settings') }}</span></a></li>
			<li><a href="{{ backpack_url('backup') }}"><i class="fa fa-hdd-o" aria-hidden="true"></i> <span>{{ trans('general.backups') }}</span></a></li>
			<li><a href="{{ backpack_url('log') }}"><i class="fa fa-terminal" aria-hidden="true"></i> <span>{{ trans('general.logs') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End Configuration -->